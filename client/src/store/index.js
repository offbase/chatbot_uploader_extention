import conversation from './conversation';
import conversations from './conversations';
import endpoints from './endpoints';
import user from './user';
import text from './text';
import submission from './submission';
import search from './search';
import preset from './preset';
import token from './token';
import lang from './language';
// TODO - custom
import upload from './upload';
import password from './password'
import slug from './slug';
import filetype from './filetype';

export default {
  ...conversation,
  ...conversations,
  ...endpoints,
  ...user,
  ...text,
  ...submission,
  ...search,
  ...preset,
  ...token,
  ...lang,
  // TODO - custom
  ...upload,
  ...password,
  ...slug,
  ...filetype
};
