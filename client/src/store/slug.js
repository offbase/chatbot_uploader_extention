import { atom } from 'recoil';

const slug = atom({
  key: 'slug',
  default: false,
});

export default {
  slug,
};
