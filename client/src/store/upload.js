import { atom } from 'recoil';

const isUploading = atom({
  key: 'isUploading',
  default: false,
});


export default {
  isUploading,
};
