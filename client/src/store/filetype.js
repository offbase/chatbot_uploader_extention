import { atom } from 'recoil';

const filetype = atom({
  key: 'filetype',
  default: false,
});

export default {
  filetype
};
